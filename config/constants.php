<?php
return [

    'bicec' => [
        'initials' => 'bicec',
        'comcode' => '81222',
        'email_name' => 'Payment Output File 81222 from KTB',
    ],

    'iveb' => [
        'initials' => 'iveb',
        'comcode' => '80923',
        'email_name' => 'Payment Output File_80923 from KTB',
    ],

    'nevia' => [
        'initials' => 'nevia',
        'comcode' => '80872',
        'email_name' => 'Payment Output File_80872 from KTB',
    ],

    'nivea' => [
        'initials' => 'nivea',
        'comcode' => '81177',
        'email_name' => 'Payment Output File_81177 from KTB',
    ],

    'ntckk' => [
        'initials' => 'ntckk',
        'comcode' => '81158',
        'email_name' => 'Payment Output File_81158 from KTB',
    ],

    'pbtc' => [
        'initials' => 'pbtc',
        'comcode' => '81134',
        'email_name' => 'Payment Output File_81134 from KTB',
    ],

    'pktc' => [
        'initials' => 'pktc',
        'comcode' => '81250',
        'email_name' => 'Payment Output File_81250 from KTB',
    ],

    'skntc' => [
        'initials' => 'skntc',
        'comcode' => '8806',
        'email_name' => 'Payment Output File_8806 from KTB',
    ],

    'sstc' => [
        'initials' => 'sstc',
        'comcode' => '80707',
        'email_name' => 'Payment Output File_80707 from KTB',
    ],

    'trang' => [
        'initials' => 'technictrang',
        'comcode' => '81164',
        'email_name' => 'Payment Output File_81164 from KTB',
    ],

    'vei5' => [
        'initials' => 'ivec5',
        'comcode' => '81034',
        'email_name' => 'Payment Output File_81034 from KTB',
    ],

    'vein3' => [
        'initials' => 'vein3',
        'comcode' => '80756',
        'email_name' => 'Payment Output File_80756 from KTB',
    ],

    'irc' =>[
        'email_name' => 'Payment Output File 23923 from KTB'
    ],

];