<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//bicec 
Route::get('/imap/bicec','Api\Payment\BICECController@updatePaymentBICEC');

//iveb
Route::get('/imap/iveb','Api\Payment\IVEBController@updatePaymentIVEB');

//nevia
Route::get('/imap/nevia','Api\Payment\NEVIAController@updatePaymentNEVIA');

//nivea
Route::get('/imap/nivea','Api\Payment\NIVEAController@updatePaymentNIVEA');

//ntckk
Route::get('/imap/ntckk','Api\Payment\NTCKKController@updatePaymentNTCKK');

//pbtc
Route::get('/imap/pbtc','Api\Payment\PBTCController@updatePaymentPBTC');

//pktc
Route::get('/imap/pktc','Api\Payment\PKTCController@updatePaymentPKTC');
Route::get('/imap/pktc/irc','Api\Payment\PKTCController@updatePaymentIrcPKTC');

//skntc
Route::get('/imap/skntc','Api\Payment\SKNTCController@updatePaymentSKNTC');

//sstc
Route::get('/imap/sstc','Api\Payment\SSTCController@updatePaymentSSTC');

//trang
Route::get('/imap/trang','Api\Payment\TRANGController@updatePaymentTRANG');

//vei5
Route::get('/imap/vei5','Api\Payment\VEI5Controller@updatePaymentVEI5');

//vein3
Route::get('/imap/vein3','Api\Payment\VEIN3Controller@updatePaymentVEIN3');

//test connect
Route::get('/imap/test-connect','Api\Payment\testCronController@testConnect');

//estudent notify
Route::get('/estudentNotify','Api\LineNotifyController@estudentNotify');


