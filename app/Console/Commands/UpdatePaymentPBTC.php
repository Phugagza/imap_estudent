<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdatePaymentPBTC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:updatepaymentPBTC';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Payment PBTC';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //pbtc
        app()->call('App\Http\Controllers\Api\Payment\PBTCController@updatePaymentPBTC');
    }
}
