<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdatePaymentVEI5 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:updatepaymentVEI5';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Payment VEI5';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //vei5
        app()->call('App\Http\Controllers\Api\Payment\VEI5Controller@updatePaymentVEI5');
    }
}
