<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LineNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:linenotify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'notify register_start_date && register_end_date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app()->call('App\Http\Controllers\Api\LineNotifyController@estudentNotify');
    }
}
