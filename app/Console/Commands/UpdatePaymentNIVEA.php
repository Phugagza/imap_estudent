<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdatePaymentNIVEA extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:updatepaymentNIVEA';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Payment NIVEA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //nivea
        app()->call('App\Http\Controllers\Api\Payment\NIVEAController@updatePaymentNIVEA');
    }
}
