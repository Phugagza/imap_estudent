<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdatePaymentPKTC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:updatepaymentPKTC';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Payment PKTC';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         //pktc
         app()->call('App\Http\Controllers\Api\Payment\PKTCController@updatePaymentPKTC');
         app()->call('App\Http\Controllers\Api\Payment\PKTCController@updatePaymentIrcPKTC');
    }
}
