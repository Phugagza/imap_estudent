<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdatePaymentIVEB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:updatepaymentIVEB';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Payment IVEB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         //iveb
         app()->call('App\Http\Controllers\Api\Payment\IVEBController@updatePaymentIVEB');
    }
}
