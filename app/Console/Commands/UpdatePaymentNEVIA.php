<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdatePaymentNEVIA extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:updatepaymentNEVIA';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Payment NEVIA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //nevia
        app()->call('App\Http\Controllers\Api\Payment\NEVIAController@updatePaymentNEVIA');
    }
}
