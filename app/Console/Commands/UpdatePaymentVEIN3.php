<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdatePaymentVEIN3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:updatepaymentVEIN3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Payment VEIN3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         //vein3
        app()->call('App\Http\Controllers\Api\Payment\VEIN3Controller@updatePaymentVEIN3');
    }
}
