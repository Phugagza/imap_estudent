<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UpdatePaymentBICEC::class,
        Commands\UpdatePaymentIVEB::class,
        Commands\UpdatePaymentNEVIA::class,
        Commands\UpdatePaymentNIVEA::class,
        Commands\UpdatePaymentNTCKK::class,
        Commands\UpdatePaymentPBTC::class,
        Commands\UpdatePaymentPKTC::class,
        Commands\UpdatePaymentSKNTC::class,
        Commands\UpdatePaymentSSTC::class,
        Commands\UpdatePaymentTRANG::class,
        Commands\UpdatePaymentVEI5::class,
        Commands\UpdatePaymentVEIN3::class,
        Commands\LineNotify::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('cron:updatepaymentBICEC')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentIVEB')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentNEVIA')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentNIVEA')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentNTCKK')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentPBTC')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentPKTC')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentSKNTC')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentSSTC')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentTRANG')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentVEI5')->dailyAt('6:00');
        $schedule->command('cron:updatepaymentVEIN3')->dailyAt('6:00');

        $schedule->command('cron:linenotify')->dailyAt('9:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
