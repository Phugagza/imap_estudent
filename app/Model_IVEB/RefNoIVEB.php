<?php

namespace App\Model_IVEB;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefNoIVEB extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'iveb';
    protected $table = 'ref_nos';
}
