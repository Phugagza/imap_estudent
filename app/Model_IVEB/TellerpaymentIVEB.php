<?php

namespace App\Model_IVEB;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentIVEB extends Model
{
    protected $connection = 'iveb';
    protected $table = 'tellerpayments';
    public $timestamps = false;
}
