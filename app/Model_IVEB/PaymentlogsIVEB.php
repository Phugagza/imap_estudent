<?php

namespace App\Model_IVEB;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsIVEB extends Model
{
    protected $connection = 'iveb';
    protected $table = 'update_payment_logs';
}
