<?php

namespace App\Model_IVEB;

use Illuminate\Database\Eloquent\Model;

class PaidStatusIVEB extends Model
{
    protected $connection = 'iveb';
    protected $table = 'paids_status';
}
