<?php

namespace App\Model_IVEB;

use Illuminate\Database\Eloquent\Model;

class PaidIVEB extends Model
{
    protected $connection = 'iveb';
    protected $table = 'paids';
}
