<?php

namespace App\Model_IVEB;

use Illuminate\Database\Eloquent\Model;

class UserIVEB extends Model
{
    protected $connection = 'iveb';
    protected $table = 'users';
}
