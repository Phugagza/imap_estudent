<?php

namespace App\Model_IVEB;

use Illuminate\Database\Eloquent\Model;

class ConfigIVEB extends Model
{
    protected $connection = 'iveb';
    protected $table = 'configs';
}
