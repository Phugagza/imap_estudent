<?php

namespace App\Model_PBTC;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class RefNoPBTC extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'pbtc';
    protected $table = 'ref_nos';
}
