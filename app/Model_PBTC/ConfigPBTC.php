<?php

namespace App\Model_PBTC;

use Illuminate\Database\Eloquent\Model;

class ConfigPBTC extends Model
{
    protected $connection = 'pbtc';
    protected $table = 'configs';
}
