<?php

namespace App\Model_PBTC;

use Illuminate\Database\Eloquent\Model;

class PaidPBTC extends Model
{
    protected $connection = 'pbtc';
    protected $table = 'paids';
}
