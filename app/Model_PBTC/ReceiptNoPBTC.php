<?php

namespace App\Model_PBTC;

use Illuminate\Database\Eloquent\Model;

class ReceiptNoPBTC extends Model
{
    protected $connection = 'pbtc_ebilling';
    protected $table = 'receipt_no_counts';
}
