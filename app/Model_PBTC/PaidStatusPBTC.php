<?php

namespace App\Model_PBTC;

use Illuminate\Database\Eloquent\Model;

class PaidStatusPBTC extends Model
{
    protected $connection = 'pbtc';
    protected $table = 'paids_status';
}
