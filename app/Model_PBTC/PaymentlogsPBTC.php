<?php

namespace App\Model_PBTC;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsPBTC extends Model
{
    protected $connection = 'pbtc';
    protected $table = 'update_payment_logs';
}
