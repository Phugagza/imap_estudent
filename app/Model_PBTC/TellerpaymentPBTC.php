<?php

namespace App\Model_PBTC;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentPBTC extends Model
{
    protected $connection = 'pbtc';
    protected $table = 'tellerpayments';
    public $timestamps = false;
}
