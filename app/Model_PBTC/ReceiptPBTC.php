<?php

namespace App\Model_PBTC;

use Illuminate\Database\Eloquent\Model;

class ReceiptPBTC extends Model
{
    protected $connection = 'pbtc';
    protected $table = 'receipts';
}
