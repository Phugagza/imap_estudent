<?php

namespace App\Model_PBTC;

use Illuminate\Database\Eloquent\Model;

class UserPBTC extends Model
{
    protected $connection = 'pbtc';
    protected $table = 'users';
}
