<?php

namespace App\Model_NEVIA;

use Illuminate\Database\Eloquent\Model;

class ReceiptNEVIA extends Model
{
    protected $connection = 'nevia';
    protected $table = 'receipts';
}
