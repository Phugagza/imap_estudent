<?php

namespace App\Model_NEVIA;

use Illuminate\Database\Eloquent\Model;

class ReceiptNoNEVIA extends Model
{
    protected $connection = 'nevia';
    protected $table = 'receipt_no_counts';
}
