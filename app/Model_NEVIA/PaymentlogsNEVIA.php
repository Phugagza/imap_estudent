<?php

namespace App\Model_NEVIA;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsNEVIA extends Model
{
    protected $connection = 'nevia';
    protected $table = 'update_payment_logs';
}
