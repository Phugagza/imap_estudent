<?php

namespace App\Model_NEVIA;

use Illuminate\Database\Eloquent\Model;

class ConfigNEVIA extends Model
{
    protected $connection = 'nevia';
    protected $table = 'configs';

}
