<?php

namespace App\Model_NEVIA;

use Illuminate\Database\Eloquent\Model;

class UserNEVIA extends Model
{
    protected $connection = 'nevia';
    protected $table = 'users';
}
