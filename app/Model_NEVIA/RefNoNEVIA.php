<?php

namespace App\Model_NEVIA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class RefNoNEVIA extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'nevia';
    protected $table = 'ref_nos';
}
