<?php

namespace App\Model_NEVIA;

use Illuminate\Database\Eloquent\Model;

class PaidNEVIA extends Model
{
    protected $connection = 'nevia';
    protected $table = 'paids';
}
