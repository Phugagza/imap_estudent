<?php

namespace App\Model_NEVIA;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentNEVIA extends Model
{
    protected $connection = 'nevia';
    protected $table = 'tellerpayments';
    public $timestamps = false;

}
