<?php

namespace App\Model_NEVIA;

use Illuminate\Database\Eloquent\Model;

class PaidStatusNEVIA extends Model
{
    protected $connection = 'nevia';
    protected $table = 'paids_status';
}
