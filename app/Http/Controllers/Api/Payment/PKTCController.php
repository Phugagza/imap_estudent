<?php

namespace App\Http\Controllers\Api\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Webklex\IMAP\Client;
use Webklex\IMAP\Support\FolderCollection;
use Webklex\IMAP\Folder;
use Webklex\IMAP\Support\MessageCollection;
use Webklex\IMAP\Message;
use Webklex\IMAP\Query\Query;
use App\Project;
use Carbon\Carbon;
use App\Model_PKTC\UserPKTC;
use App\Model_PKTC\RefNoPKTC;
use App\Model_PKTC\PaymentlogsPKTC;
use App\Model_PKTC\PaidPKTC;
use App\Model_PKTC\PaidStatusPKTC;
use App\Model_PKTC\TellerpaymentPKTC;
use App\Model_PKTC\ConfigPKTC;
use App\Model_PKTC\ReceiptPKTC;
use App\Model_PKTC\ReceiptNoPKTC;

use App\Model_PKTC\Model_PKTC_IRC\RefNoIrcPKTC;
use App\Model_PKTC\Model_PKTC_IRC\PaymentlogsIrcPKTC;
use App\Model_PKTC\Model_PKTC_IRC\PaidIrcPKTC;
use App\Model_PKTC\Model_PKTC_IRC\PaidStatusIrcPKTC;
use App\Model_PKTC\Model_PKTC_IRC\TellerpaymentIrcPKTC;


class PKTCController extends Controller
{

   public function updatePaymentPKTC(){
    //change db_name to current round
    $getDBPKTC = new Project();
   if ($getDBPKTC -> getCurrentDBPKTC() -> db_name == null){
       $messages = "อัพเดทการเงินไม่สำเร็จ เนื่องจากไม่พบ DB ของ ".config('constants.pktc.initials');
       lineNotify($messages);
   }
    config(['database.connections.pktc.database' => $getDBPKTC -> getCurrentDBPKTC() -> db_name]);
//   dd(
//       UserPKTC::all(),
//       RefNoPKTC::all(),
//       PaymentlogsPKTC::all(),
//       PaidPKTC::all(),
//       PaidStatusPKTC::all(),
//       TellerpaymentPKTC::all(),
//       ConfigPKTC::all(),
//       ReceiptNoPKTC::all(),
//       ReceiptPKTC::all()
//   );
    $oClient = \Webklex\IMAP\Facades\Client::account('gmail');
    $oClient->connect();
    $getInbox = $oClient->getFolder('INBOX');

    //get message by date && specific subject name
    $oMessages = $getInbox->query()->on(Carbon::today())->subject(config('constants.pktc.email_name'))->get();

    //get message by unread
    // $oMessages = $getInbox->getUnseenMessages();

   
    foreach($oMessages as $oMessage){
        
        $oMessage->getAttachments()->each(function ($oAttachment) use ($oMessage) {
            $count = 0;
            $RefFail = 0;
            $RefnoSuccess = 0;
            $PaidSuccess = 0;
            $hasData = false;
            $config_pktc=ConfigPKTC::first();
            foreach(preg_split("/((\r?\n)|(\r\n?))/", $oAttachment->content) as $line){

                // Header File
                if (trim(substr($line,0,1)) == "H") {
                    $CompanyName = trim(substr($line,20,40));
                    $CompanyCode = trim(substr($line,68,5));
                }
              
                //Data Payment 
                if (trim(substr($line,0,1)) == "D") {
                    $BankCode = trim(substr($line,7,3));
                    $AccountNumber = trim(substr($line,10,10));
                    $TransDate = trim(substr($line,20,8));
                    $TransDate = substr($TransDate,4,4).substr($TransDate,2,2).substr($TransDate,0,2);
                    $TransTime = trim(substr($line,28,6));
                    $CustomerName = trim(substr($line,34,50));
                    $RefNo1 = trim(substr($line,84,14));
                    $RefNo2 = trim(substr($line,104,13));
                    $TermBranch = trim(substr($line,144,3));
                    $TellerID = trim(substr($line,148,4));
                    $CreditDeBit = trim(substr($line,152,1));
                    $Type = trim(substr($line,153,10));
                    $ChequeNo = trim(substr($line,163,8));
                    $Amount = trim(substr($line,170,6));
                    $ChqBankCode = trim(substr($line,176,3));
                    
                    //save raw data on bank file 
                    $tellerpayment = new TellerpaymentPKTC();
                    $tellerpayment -> CompanyCode = $CompanyCode ? $CompanyCode : '';
                    $tellerpayment -> TextFile = $TransDate ? $TransDate : '';
                    $tellerpayment -> BankCode = $BankCode ? $BankCode : '';
                    $tellerpayment -> AccountNumber = $AccountNumber ? $AccountNumber : '';
                    $tellerpayment -> TransDate = $TransDate ? $TransDate : '';
                    $tellerpayment -> TransTime = $TransTime ? $TransTime : '';
                    $tellerpayment -> CustomerName = $CustomerName ? convert_tis620_utf8($CustomerName) : '';
                    $tellerpayment -> RefNo1 = $RefNo1 ? $RefNo1 : '';
                    $tellerpayment -> TermBranch = $TermBranch ? $TermBranch : '';
                    $tellerpayment -> TellerID = $TellerID ? $TellerID : '';
                    $tellerpayment -> CreditDeBit = $CreditDeBit ? $CreditDeBit : '';
                    $tellerpayment -> Type = $Type ? $Type : '';
                    $tellerpayment -> ChequeNo = $ChequeNo ? $ChequeNo : '';
                    $tellerpayment -> Amount = $Amount ? $Amount / 100 : '';
                    $tellerpayment -> ChqBankCode = $RefNo2 ? $RefNo2 : '';
                    $tellerpayment -> save();

                    //update payment data in site
                    $ref_no_pktc = RefNoPKTC::where('no', $RefNo1)->where('status_refno',1)->first();
                    $student = UserPKTC::where('code', (int)$RefNo2)->first();
                    if($ref_no_pktc && $student){
                            if($ref_no_pktc->paystatus == 1){
                                $log = new PaymentlogsPKTC;
                                $log->refno1 = $RefNo1;
                                $log->refno2 = $RefNo2;
                                $log->date = $TransDate;
                                $log->time = $TransTime;
                                $log->pay = $Amount;
                                $log->status = "Already";
                                $log->save();
                                $RefFail++;
                            }else{
                                $ref_no_pktc->paystatus = 1;
                                $ref_no_pktc->paydate = $TransDate;
                                $ref_no_pktc->paytime = $TransTime;
                                $ref_no_pktc->pay = $Amount / 100;
                                $ref_no_pktc->save();
                                $count++;

                                $paid = new PaidPKTC;
                                $paid->paydate = $TransDate;
                                $paid->paytime = $TransTime;
                                $paid->ref1 = $RefNo1;
                                $paid->ref2 = $RefNo2;
                                $paid->pay = $Amount;
                                $paid->save();
                                $PaidSuccess++;

                                $log = new PaymentlogsPKTC;
                                $log->refno1 = $RefNo1;
                                $log->refno2 = $RefNo2;
                                $log->date = $TransDate;
                                $log->time = $TransTime;
                                $log->pay = $Amount;
                                $log->status = "Success";
                                $log->save();
                                $RefnoSuccess++;

                                $receipt_no = ReceiptNoPKTC::first();
                                if($receipt_no->receipt_available == 0){
                                        
                                }else{

                                    $no = $receipt_no->receipt_last_no;

                                    $receipt = new ReceiptPKTC;
                                    $receipt->user_id = $student->id;
                                    $receipt->receipt_no = $no;
                                    $receipt->receipt_date = $TransDate;
                                    $receipt->receipt_time = $TransTime;
                                    $receipt->receipt_price = $Amount;
                                    $receipt->receipt_refno1 = $RefNo1;
                                    $receipt->is_late = $ref_no_pktc->is_late;
                                    $receipt->period = $ref_no_pktc->period;
                                    $receipt->save();

                                    $receipt_no->receipt_last_no = $no + 1;
                                    $receipt_no->receipt_available = $receipt_no->receipt_available - 1;
                                    $receipt_no->save();

                                    $refno_receipt = RefNoPKTC::find($ref_no_pktc->id);
                                    $refno_receipt->receipt_id = $no;
                                    $refno_receipt->save();

                                }
                            }
                        }else{
                            $log = new PaymentlogsPKTC;
                            $log->refno1 = $RefNo1;
                            $log->refno2 = $RefNo2;
                            $log->date = $TransDate;
                            $log->time = $TransTime;
                            $log->pay = $Amount;
                            $log->status = "Fail";
                            $log->save();
                            $RefFail++;
                        }
                        $hasData = true;
                }
                
            }
            if($hasData){
                $status = new PaidStatusPKTC;
                $status->success = $RefnoSuccess ? $RefnoSuccess : '';
                $status->fail = $RefFail;
                $status->date = $TransDate;
                $status->year_id = $config_pktc -> year_id;
                $status->term_id = $config_pktc -> term_id;
                $status->save();
                if($RefFail > 0){
                    $messages = "{$config_pktc -> name} อัพเดทการเงินไม่สำเร็จจำนวน {$RefFail} คน ({$TransDate})";
                    lineNotify($messages);
                }
            }
        });
           
    }
    return "true";
   }

   public function updatePaymentIrcPKTC(){
    //change db_name to current round
    $getDBPKTC = new Project();
       if ($getDBPKTC -> getCurrentDBPKTC() -> db_name == null){
           $messages = "อัพเดทการเงินไม่สำเร็จ เนื่องจากไม่พบ DB ของ ".config('constants.pktc.initials');
           lineNotify($messages);
       }
    config(['database.connections.pktc.database' => $getDBPKTC -> getCurrentDBPKTC() -> db_name]);

    $oClient = \Webklex\IMAP\Facades\Client::account('gmail');
    $oClient->connect();
    $getInbox = $oClient->getFolder('INBOX');

    //get message by date && specific subject name
    $oMessages = $getInbox->query()->on(Carbon::today())->subject(config('constants.irc.email_name'))->get();
    foreach($oMessages as $oMessage){
        
        $oMessage->getAttachments()->each(function ($oAttachment) use ($oMessage) {
            $count = 0;
            $RefFail = 0;
            $RefnoSuccess = 0;
            $PaidSuccess = 0;
            $hasData = false;
            $config_pktc=ConfigPKTC::first();
            foreach(preg_split("/((\r?\n)|(\r\n?))/", $oAttachment->content) as $line){

                // Header File
                if (trim(substr($line,0,1)) == "H") {
                    $CompanyName = trim(substr($line,20,40));
                    $CompanyCode = trim(substr($line,68,5));
                }
              
                //Data Payment 
                if (trim(substr($line,0,1)) == "D") {
                    //str ref1 to check comcode each site
                   if(trim(substr($line,84,5)) == $config_pktc -> company_code){
                    $BankCode = trim(substr($line,7,3));
                    $AccountNumber = trim(substr($line,10,10));
                    $TransDate = trim(substr($line,20,8));
                    $TransDate = substr($TransDate,4,4).substr($TransDate,2,2).substr($TransDate,0,2);
                    $TransTime = trim(substr($line,28,6));
                    $CustomerName = trim(substr($line,34,50));
                    $RefNo1 = trim(substr($line,84,14));
                    $RefNo2 = trim(substr($line,104,13));
                    $TermBranch = trim(substr($line,144,3));
                    $TellerID = trim(substr($line,148,4));
                    $CreditDeBit = trim(substr($line,152,1));
                    $Type = trim(substr($line,153,10));
                    $ChequeNo = trim(substr($line,163,8));
                    $Amount = trim(substr($line,170,6));
                    $ChqBankCode = trim(substr($line,176,3));
                    
                    //save raw data on bank file 
                    $tellerpayment = new TellerpaymentIrcPKTC();
                    $tellerpayment -> CompanyCode = $CompanyCode ? $CompanyCode : '';
                    $tellerpayment -> TextFile = $TransDate ? $TransDate : '';
                    $tellerpayment -> BankCode = $BankCode ? $BankCode : '';
                    $tellerpayment -> AccountNumber = $AccountNumber ? $AccountNumber : '';
                    $tellerpayment -> TransDate = $TransDate ? $TransDate : '';
                    $tellerpayment -> TransTime = $TransTime ? $TransTime : '';
                    $tellerpayment -> CustomerName = $CustomerName ? convert_tis620_utf8($CustomerName) : '';
                    $tellerpayment -> RefNo1 = $RefNo1 ? $RefNo1 : '';
                    $tellerpayment -> TermBranch = $TermBranch ? $TermBranch : '';
                    $tellerpayment -> TellerID = $TellerID ? $TellerID : '';
                    $tellerpayment -> CreditDeBit = $CreditDeBit ? $CreditDeBit : '';
                    $tellerpayment -> Type = $Type ? $Type : '';
                    $tellerpayment -> ChequeNo = $ChequeNo ? $ChequeNo : '';
                    $tellerpayment -> Amount = $Amount ? $Amount / 100 : '';
                    $tellerpayment -> ChqBankCode = $RefNo2 ? $RefNo2 : '';
                    $tellerpayment -> save();

                    //update payment data in site
                    $ref_no_pktc = RefNoIrcPKTC::where('no', $RefNo1)->where('status_refno',1)->first();
                    $student = UserPKTC::where('code', (int)$RefNo2)->first();
                    if($ref_no_pktc && $student){
                            if($ref_no_pktc->paystatus == 1){
                                $log = new PaymentlogsIrcPKTC;
                                $log->refno1 = $RefNo1;
                                $log->refno2 = $RefNo2;
                                $log->date = $TransDate;
                                $log->time = $TransTime;
                                $log->pay = $Amount;
                                $log->status = "Already";
                                $log->save();
                                $RefFail++;
                            }else{
                                $ref_no_pktc->paystatus = 1;
                                $ref_no_pktc->paydate = $TransDate;
                                $ref_no_pktc->paytime = $TransTime;
                                $ref_no_pktc->pay = $Amount / 100;
                                $ref_no_pktc->save();
                                $count++;

                                $paid = new PaidIrcPKTC;
                                $paid->paydate = $TransDate;
                                $paid->paytime = $TransTime;
                                $paid->ref1 = $RefNo1;
                                $paid->ref2 = $RefNo2;
                                $paid->pay = $Amount;
                                $paid->save();
                                $PaidSuccess++;

                                $log = new PaymentlogsIrcPKTC;
                                $log->refno1 = $RefNo1;
                                $log->refno2 = $RefNo2;
                                $log->date = $TransDate;
                                $log->time = $TransTime;
                                $log->pay = $Amount;
                                $log->status = "Success";
                                $log->save();
                                $RefnoSuccess++;
                            }
                        }else{
                            $log = new PaymentlogsIrcPKTC;
                            $log->refno1 = $RefNo1;
                            $log->refno2 = $RefNo2;
                            $log->date = $TransDate;
                            $log->time = $TransTime;
                            $log->pay = $Amount;
                            $log->status = "Fail";
                            $log->save();
                            $RefFail++;
                        }
                        $hasData = true;
                }
              }
            }
            if($hasData){
                $status = new PaidStatusIrcPKTC;
                $status->success = $RefnoSuccess ? $RefnoSuccess : '';
                $status->fail = $RefFail;
                $status->date = $TransDate;
                $status->year_id = $config_pktc -> year_id;
                $status->term_id = $config_pktc -> term_id;
                $status->save();
                if($RefFail > 0){
                    $messages = "{$config_pktc -> name} อัพเดทการเงินค่าธรรมเนียมไม่สำเร็จจำนวน {$RefFail} คน ({$TransDate})";
                    lineNotify($messages);
                }
            }
        });      
    }
    return "true";
   }

  
}
