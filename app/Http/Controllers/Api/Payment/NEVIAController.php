<?php

namespace App\Http\Controllers\Api\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Webklex\IMAP\Folder;
use Webklex\IMAP\Support\MessageCollection;
use Webklex\IMAP\Message;
use Webklex\IMAP\Query\Query;
use App\Project;
use Carbon\Carbon;
use App\Model_NEVIA\UserNEVIA;
use App\Model_NEVIA\RefNoNEVIA;
use App\Model_NEVIA\PaymentlogsNEVIA;
use App\Model_NEVIA\PaidNEVIA;
use App\Model_NEVIA\PaidStatusNEVIA;
use App\Model_NEVIA\TellerpaymentNEVIA;
use App\Model_NEVIA\ConfigNEVIA;
use App\Model_NEVIA\ReceiptNEVIA;
use App\Model_NEVIA\ReceiptNoNEVIA;

class NEVIAController extends Controller
{
    public function updatePaymentNEVIA()
    {
        
    //change db_name to current round
    $getDBNEVIA = new Project();
    if ($getDBNEVIA -> getCurrentDBNEVIA() -> db_name == null){
        $messages = "อัพเดทการเงินไม่สำเร็จ เนื่องจากไม่พบ DB ของ ".config('constants.nevia.initials');
        lineNotify($messages);
    }
    config(['database.connections.nevia.database' => $getDBNEVIA -> getCurrentDBNEVIA() -> db_name]);

//    dd(
//        UserNEVIA::all(),
//        RefNoNEVIA::all(),
//        PaymentlogsNEVIA::all(),
//        PaidNEVIA::all(),
//        PaidStatusNEVIA::all(),
//        TellerpaymentNEVIA::all(),
//        ConfigNEVIA::all()
//    );
    $oClient = \Webklex\IMAP\Facades\Client::account('gmail');
    $oClient->connect();
    $getInbox = $oClient->getFolder('INBOX');

    //get message by date && specific subject name
    $oMessages = $getInbox->query()->on(Carbon::today())->subject(config('constants.nevia.email_name'))->get();
   
    foreach($oMessages as $oMessage){
        
        $oMessage->getAttachments()->each(function ($oAttachment) use ($oMessage) {
            $count = 0;
            $RefFail = 0;
            $RefnoSuccess = 0;
            $PaidSuccess = 0;
            $hasData = false;
            $config_nevia=ConfigNEVIA::first();
            foreach(preg_split("/((\r?\n)|(\r\n?))/", $oAttachment->content) as $line){

                // Header File
                if (trim(substr($line,0,1)) == "H") {
                    $CompanyName = trim(substr($line,20,40));
                    $CompanyCode = trim(substr($line,68,5));
                }
              
                //Data Payment 
                if (trim(substr($line,0,1)) == "D") {
                    $BankCode = trim(substr($line,7,3));
                    $AccountNumber = trim(substr($line,10,10));
                    $TransDate = trim(substr($line,20,8));
                    $TransDate = substr($TransDate,4,4).substr($TransDate,2,2).substr($TransDate,0,2);
                    $TransTime = trim(substr($line,28,6));
                    $CustomerName = trim(substr($line,34,50));
                    $RefNo1 = trim(substr($line,84,14));
                    $RefNo2 = trim(substr($line,104,13));
                    $TermBranch = trim(substr($line,144,3));
                    $TellerID = trim(substr($line,148,4));
                    $CreditDeBit = trim(substr($line,152,1));
                    $Type = trim(substr($line,153,10));
                    $ChequeNo = trim(substr($line,163,8));
                    $Amount = trim(substr($line,170,6));
                    $ChqBankCode = trim(substr($line,176,3));
                    
                    //save raw data on bank file 
                    $tellerpayment = new TellerpaymentNEVIA();
                    $tellerpayment -> CompanyCode = $CompanyCode ? $CompanyCode : '';
                    $tellerpayment -> TextFile = $TransDate ? $TransDate : '';
                    $tellerpayment -> BankCode = $BankCode ? $BankCode : '';
                    $tellerpayment -> AccountNumber = $AccountNumber ? $AccountNumber : '';
                    $tellerpayment -> TransDate = $TransDate ? $TransDate : '';
                    $tellerpayment -> TransTime = $TransTime ? $TransTime : '';
                    $tellerpayment -> CustomerName = $CustomerName ? convert_tis620_utf8($CustomerName) : '';
                    $tellerpayment -> RefNo1 = $RefNo1 ? $RefNo1 : '';
                    $tellerpayment -> TermBranch = $TermBranch ? $TermBranch : '';
                    $tellerpayment -> TellerID = $TellerID ? $TellerID : '';
                    $tellerpayment -> CreditDeBit = $CreditDeBit ? $CreditDeBit : '';
                    $tellerpayment -> Type = $Type ? $Type : '';
                    $tellerpayment -> ChequeNo = $ChequeNo ? $ChequeNo : '';
                    $tellerpayment -> Amount = $Amount ? $Amount / 100 : '';
                    $tellerpayment -> ChqBankCode = $RefNo2 ? $RefNo2 : '';
                    $tellerpayment -> save();

                    //update payment data in site
                    $ref_no_nevia = RefNoNEVIA::where('no', $RefNo1)->where('status_refno',1)->first();
                    $student = UserNEVIA::where('code', (int)$RefNo2)->first();
                    if($ref_no_nevia && $student){
                            if($ref_no_nevia->paystatus == 1){
                                $log = new PaymentlogsNEVIA;
                                $log->refno1 = $RefNo1;
                                $log->refno2 = $RefNo2;
                                $log->date = $TransDate;
                                $log->time = $TransTime;
                                $log->pay = $Amount;
                                $log->status = "Already";
                                $log->save();
                                $RefFail++;
                            }else{
                                $ref_no_nevia->paystatus = 1;
                                $ref_no_nevia->paydate = $TransDate;
                                $ref_no_nevia->paytime = $TransTime;
                                $ref_no_nevia->pay = $Amount / 100;
                                $ref_no_nevia->save();
                                $count++;

                                $paid = new PaidNEVIA;
                                $paid->paydate = $TransDate;
                                $paid->paytime = $TransTime;
                                $paid->ref1 = $RefNo1;
                                $paid->ref2 = $RefNo2;
                                $paid->pay = $Amount;
                                $paid->save();
                                $PaidSuccess++;

                                $log = new PaymentlogsNEVIA;
                                $log->refno1 = $RefNo1;
                                $log->refno2 = $RefNo2;
                                $log->date = $TransDate;
                                $log->time = $TransTime;
                                $log->pay = $Amount;
                                $log->status = "Success";
                                $log->save();
                                $RefnoSuccess++;
                            }
                        }else{
                            $log = new PaymentlogsNEVIA;
                            $log->refno1 = $RefNo1;
                            $log->refno2 = $RefNo2;
                            $log->date = $TransDate;
                            $log->time = $TransTime;
                            $log->pay = $Amount;
                            $log->status = "Fail";
                            $log->save();
                            $RefFail++;
                        }
                        $hasData = true;
                }
                
            }
            if($hasData){
                $status = new PaidStatusNEVIA;
                $status->success = $RefnoSuccess ? $RefnoSuccess : '';
                $status->fail = $RefFail;
                $status->date = $TransDate;
                $status->year_id = $config_nevia -> year_id;
                $status->term_id = $config_nevia -> term_id;
                $status->save();
                if($RefFail > 0){
                    $messages = "{$config_nevia -> name} อัพเดทการเงินไม่สำเร็จจำนวน {$RefFail} คน ({$TransDate})";
                    lineNotify($messages);
                }
            }
        });
           
    }

    }
}
