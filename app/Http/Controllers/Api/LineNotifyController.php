<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ixudra\Curl\Facades\Curl;
use App\Project;
use App\College;

class LineNotifyController extends Controller
{
   public function estudentNotify(){
      $projects = Project::with('college')->orderBy('id',"desc")->get();
      foreach ($projects as $project){
          $date_now =Carbon::now();
          // change format date in project
          $sub_regis_start = explode('/',$project->regis_start);
          $resub_regis_start = ($sub_regis_start[2]-543)."-".$sub_regis_start[1]."-".$sub_regis_start[0];
          $sub_regis_end = explode('/',$project->regis_end);
          $resub_regis_end = ($sub_regis_end[2]-543)."-".$sub_regis_end[1]."-".$sub_regis_end[0];

          $register_start_date =  new Carbon($resub_regis_start);
          $register_end_date = new Carbon($resub_regis_end);
          $count_down_start_date =  $date_now->startOfDay()->diffInDays($register_start_date->startOfDay());
          $count_down_end_date =  $date_now->startOfDay()->diffInDays($register_end_date->startOfDay());
//          dd($register_start_date -> format('l'));//Saturday Sunday
          if($date_now->addDay(1)->toDateString() == $register_start_date->toDateString()){
             $message = $project ->college-> college." ( ".$project->project_name." )".
                        "\n ลงทะเบียนวันที่ ".ConvertThaiDate($project ->regis_start)."อีก {$count_down_start_date} วัน".
                        "\n เปิดชำระเงินวันที่ ".ConvertThaiDate($project ->pay_start).
                        "\n".
                        "ตรวจสอบได้ที่ http://203.151.93.149/".$project->initials."/index.php/login";
             Curl::to('https://notify-api.line.me/api/notify')
                  ->withHeaders(array(
                      'Content-Type' => 'application/x-www-form-urlencoded',
                      'Authorization' => "Bearer FEiMaltY7zod0MUecJUyugFy6THzrdYRcf68Wti8W0I"
                  ))
                  ->withData(array(
                      'message' => $message,
                  ))
                  ->post();
              }

          if($date_now->addDay(1)->toDateString() == $register_end_date->toDateString()){
              $message = $project ->college-> college." ( ".$project->project_name." )".
                  "\n ปิดทะเบียนวันที่ ".ConvertThaiDate($project ->regis_end)."อีก {$count_down_end_date} วัน".
                  "\n ปิดชำระเงินวันที่ ".ConvertThaiDate($project ->pay_end).
                  "\n".
                  "ตรวจสอบได้ที่ http://203.151.93.149/".$project->initials."/index.php/login";
                Curl::to('https://notify-api.line.me/api/notify')
                  ->withHeaders(array(
                      'Content-Type' => 'application/x-www-form-urlencoded',
                      'Authorization' => "Bearer FEiMaltY7zod0MUecJUyugFy6THzrdYRcf68Wti8W0I"
                  ))
                  ->withData(array(
                      'message' => $message,
                  ))
                  ->post();
          }
      }
   }
}
