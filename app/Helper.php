<?php
use Ixudra\Curl\Facades\Curl;
function convert_tis620_utf8($data){
    $convert = iconv("tis-620", "utf-8",$data);
    return $convert;
}
function ConvertThaiDate($date){
    $sub_date = explode('/',$date);
    $strMonthCut=array(
        "0"=>"",
        "01"=>"มกราคม",
        "02"=>"กุมภาพันธ์",
        "03"=>"มีนาคม",
        "04"=>"เมษายน",
        "05"=>"พฤษภาคม",
        "06"=>"มิถุนายน",
        "07"=>"กรกฎาคม",
        "08"=>"สิงหาคม",
        "09"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม"
    );
    $strYear = $sub_date[2];
    $strDay= $sub_date[0];
    $strMonthThai=$strMonthCut[$sub_date[1]];
    return $strDay." ". $strMonthThai." ".$strYear;
}

function lineNotify($data){

    $response = Curl::to('https://notify-api.line.me/api/notify')
        ->withHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => "Bearer FEiMaltY7zod0MUecJUyugFy6THzrdYRcf68Wti8W0I"
        ))
        ->withData(array(
            'message' => $data,
        ))
        ->post();

    $getStatus = json_decode($response);
    return $getStatus -> status;
}
