<?php

namespace App\Model_VEIN3;

use Illuminate\Database\Eloquent\Model;

class PaidStatusVEIN3 extends Model
{
    protected $connection = 'vein3';
    protected $table = 'paids_status';
}
