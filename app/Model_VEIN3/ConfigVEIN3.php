<?php

namespace App\Model_VEIN3;

use Illuminate\Database\Eloquent\Model;

class ConfigVEIN3 extends Model
{
    protected $connection = 'vein3';
    protected $table = 'configs';
}
