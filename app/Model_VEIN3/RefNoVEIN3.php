<?php

namespace App\Model_VEIN3;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefNoVEIN3 extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'vein3';
    protected $table = 'ref_nos';
}
