<?php

namespace App\Model_VEI5;

use Illuminate\Database\Eloquent\Model;

class PaidStatusVEI5 extends Model
{
    protected $connection = 'vei5';
    protected $table = 'paids_status';
}
