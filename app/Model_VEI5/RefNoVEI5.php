<?php

namespace App\Model_VEI5;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class RefNoVEI5 extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'vei5';
    protected $table = 'ref_nos';
}
