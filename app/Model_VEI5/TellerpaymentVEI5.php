<?php

namespace App\Model_VEI5;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentVEI5 extends Model
{
    protected $connection = 'vei5';
    protected $table = 'tellerpayments';
    public $timestamps = false;
}
