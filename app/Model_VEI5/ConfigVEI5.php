<?php

namespace App\Model_VEI5;

use Illuminate\Database\Eloquent\Model;

class ConfigVEI5 extends Model
{
    protected $connection = 'vei5';
    protected $table = 'configs';
}
