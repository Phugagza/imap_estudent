<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    public function project(){
        return $this->hasMany('App\Project','id','cl_id');
    }
}
