<?php

namespace App\Model_NIVEA;

use Illuminate\Database\Eloquent\Model;

class ReceiptNIVEA extends Model
{
    protected $connection = 'nivea';
    protected $table = 'receipts';
}
