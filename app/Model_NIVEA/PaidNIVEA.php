<?php

namespace App\Model_NIVEA;

use Illuminate\Database\Eloquent\Model;

class PaidNIVEA extends Model
{
    protected $connection = 'nivea';
    protected $table = 'paids';
}
