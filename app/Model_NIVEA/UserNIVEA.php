<?php

namespace App\Model_NIVEA;

use Illuminate\Database\Eloquent\Model;

class UserNIVEA extends Model
{
    protected $connection = 'nivea';
    protected $table = 'users';
}
