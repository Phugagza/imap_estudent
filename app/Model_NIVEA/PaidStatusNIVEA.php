<?php

namespace App\Model_NIVEA;

use Illuminate\Database\Eloquent\Model;

class PaidStatusNIVEA extends Model
{
    protected $connection = 'nivea';
    protected $table = 'paids_status';
}
