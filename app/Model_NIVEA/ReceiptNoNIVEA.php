<?php

namespace App\Model_NIVEA;

use Illuminate\Database\Eloquent\Model;

class ReceiptNoNIVEA extends Model
{
    protected $connection = 'nivea';
    protected $table = 'receipt_no_counts';
}
