<?php

namespace App\Model_NIVEA;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsNIVEA extends Model
{
    protected $connection = 'nivea';
    protected $table = 'update_payment_logs';
}
