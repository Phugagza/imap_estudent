<?php

namespace App\Model_NIVEA;

use Illuminate\Database\Eloquent\Model;

class ConfigNIVEA extends Model
{
    protected $connection = 'nivea';
    protected $table = 'configs';
}
