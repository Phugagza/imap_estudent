<?php

namespace App\Model_NIVEA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefNoNIVEA extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'nivea';
    protected $table = 'ref_nos';
}
