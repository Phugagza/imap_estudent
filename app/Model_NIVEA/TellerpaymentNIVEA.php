<?php

namespace App\Model_NIVEA;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentNIVEA extends Model
{
    protected $connection = 'nivea';
    protected $table = 'tellerpayments';
    public $timestamps = false;
}
