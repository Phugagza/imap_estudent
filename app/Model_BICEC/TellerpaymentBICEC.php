<?php

namespace App\Model_BICEC;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentBICEC extends Model
{
    protected $connection = 'bicec';
    protected $table = 'tellerpayments';
    public $timestamps = false;
}
