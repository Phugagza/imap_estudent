<?php

namespace App\Model_BICEC;

use Illuminate\Database\Eloquent\Model;

class PaidStatusBICEC extends Model
{
    protected $connection = 'bicec';
    protected $table = 'paids_status';
}
