<?php

namespace App\Model_BICEC;

use Illuminate\Database\Eloquent\Model;

class PaidBICEC extends Model
{
    protected $connection = 'bicec';
    protected $table = 'paids';
}
