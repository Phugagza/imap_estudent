<?php

namespace App\Model_BICEC;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsBICEC extends Model
{
    protected $connection = 'bicec';
    protected $table = 'update_payment_logs';
}
