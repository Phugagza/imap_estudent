<?php

namespace App\Model_BICEC;

use Illuminate\Database\Eloquent\Model;

class UserBICEC extends Model
{
    protected $connection = 'bicec';
    protected $table = 'users';
}
