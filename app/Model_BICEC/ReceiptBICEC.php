<?php

namespace App\Model_BICEC;

use Illuminate\Database\Eloquent\Model;

class ReceiptBICEC extends Model
{
    protected $connection = 'bicec';
    protected $table = 'receipts';
}
