<?php

namespace App\Model_BICEC;

use Illuminate\Database\Eloquent\Model;

class ConfigBICEC extends Model
{
    protected $connection = 'bicec';
    protected $table = 'configs';
}
