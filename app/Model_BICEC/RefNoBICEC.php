<?php

namespace App\Model_BICEC;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefNoBICEC extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'bicec';
    protected $table = 'ref_nos';
}
