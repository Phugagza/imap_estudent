<?php

namespace App\Model_SKNTC;

use Illuminate\Database\Eloquent\Model;

class ReceiptNoSKNTC extends Model
{
    protected $connection = 'skntc_ebilling';
    protected $table = 'receipt_no_counts';

}
