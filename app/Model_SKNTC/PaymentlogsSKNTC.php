<?php

namespace App\Model_SKNTC;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsSKNTC extends Model
{
    protected $connection = 'skntc';
    protected $table = 'update_payment_logs';
}
