<?php

namespace App\Model_SKNTC;

use Illuminate\Database\Eloquent\Model;

class PaidSKNTC extends Model
{
    protected $connection = 'skntc';
    protected $table = 'paids';
}
