<?php

namespace App\Model_SKNTC;

use Illuminate\Database\Eloquent\Model;

class PaidStatusSKNTC extends Model
{
    protected $connection = 'skntc';
    protected $table = 'paids_status';
}
