<?php

namespace App\Model_SKNTC;

use Illuminate\Database\Eloquent\Model;

class ConfigSKNTC extends Model
{
    protected $connection = 'skntc';
    protected $table = 'configs';
}
