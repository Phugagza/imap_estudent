<?php

namespace App\Model_SKNTC;

use Illuminate\Database\Eloquent\Model;

class ReceiptSKNTC extends Model
{
    protected $connection = 'skntc';
    protected $table = 'receipts';
}
