<?php

namespace App\Model_SKNTC;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefNoSKNTC extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'skntc';
    protected $table = 'ref_nos';
}
