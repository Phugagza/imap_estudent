<?php

namespace App\Model_SKNTC;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentSKNTC extends Model
{
    protected $connection = 'skntc';
    protected $table = 'tellerpayments';
    public $timestamps = false;
}
