<?php

namespace App\Model_SSTC;

use Illuminate\Database\Eloquent\Model;

class ReceiptSSTC extends Model
{
    protected $connection = 'sstc';
    protected $table = 'receipts';
}
