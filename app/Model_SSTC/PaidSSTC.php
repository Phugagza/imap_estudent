<?php

namespace App\Model_SSTC;

use Illuminate\Database\Eloquent\Model;

class PaidSSTC extends Model
{
    protected $connection = 'sstc';
    protected $table = 'paids';
}
