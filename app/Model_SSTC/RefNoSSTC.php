<?php

namespace App\Model_SSTC;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefNoSSTC extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'sstc';
    protected $table = 'ref_nos';
}
