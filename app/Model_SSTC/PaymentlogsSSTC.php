<?php

namespace App\Model_SSTC;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsSSTC extends Model
{
    protected $connection = 'sstc';
    protected $table = 'update_payment_logs';
}
