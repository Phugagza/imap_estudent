<?php

namespace App\Model_SSTC;

use Illuminate\Database\Eloquent\Model;

class UserSSTC extends Model
{
    protected $connection = 'sstc';
    protected $table = 'users';
}
