<?php

namespace App\Model_SSTC;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentSSTC extends Model
{
    protected $connection = 'sstc';
    protected $table = 'tellerpayments';
    public $timestamps = false;
}
