<?php

namespace App\Model_SSTC;

use Illuminate\Database\Eloquent\Model;

class PaidStatusSSTC extends Model
{
    protected $connection = 'sstc';
    protected $table = 'paids_status';
}
