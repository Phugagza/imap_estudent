<?php

namespace App\Model_SSTC;

use Illuminate\Database\Eloquent\Model;

class ConfigSSTC extends Model
{
    protected $connection = 'sstc';
    protected $table = 'configs';
}
