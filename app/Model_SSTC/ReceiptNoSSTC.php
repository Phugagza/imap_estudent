<?php

namespace App\Model_SSTC;

use Illuminate\Database\Eloquent\Model;

class ReceiptNoSSTC extends Model
{
    protected $connection = 'sstc';
    protected $table = 'receipt_no_counts';
}
