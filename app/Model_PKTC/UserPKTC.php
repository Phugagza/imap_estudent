<?php

namespace App\Model_PKTC;

use Illuminate\Database\Eloquent\Model;

class UserPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'users';
}
