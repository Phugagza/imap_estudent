<?php

namespace App\Model_PKTC;

use Illuminate\Database\Eloquent\Model;

class PaidStatusPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'paids_status';
}
