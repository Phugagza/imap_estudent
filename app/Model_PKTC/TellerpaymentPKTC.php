<?php

namespace App\Model_PKTC;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'tellerpayments';
    public $timestamps = false;
}
