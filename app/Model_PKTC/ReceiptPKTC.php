<?php

namespace App\Model_PKTC;

use Illuminate\Database\Eloquent\Model;

class ReceiptPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'receipts';
}
