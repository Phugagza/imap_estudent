<?php

namespace App\Model_PKTC;

use Illuminate\Database\Eloquent\Model;

class PaidPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'paids';
}
