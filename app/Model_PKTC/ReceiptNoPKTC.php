<?php

namespace App\Model_PKTC;

use Illuminate\Database\Eloquent\Model;

class ReceiptNoPKTC extends Model
{
    protected $connection = 'pktc_billing';
    protected $table = 'receipt_no_counts';
}
