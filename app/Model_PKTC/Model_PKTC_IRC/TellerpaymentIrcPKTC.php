<?php

namespace App\Model_PKTC\Model_PKTC_IRC;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentIrcPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'tellerpayment_irc';
    public $timestamps = false;
}
