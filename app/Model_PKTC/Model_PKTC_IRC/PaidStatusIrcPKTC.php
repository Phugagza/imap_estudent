<?php

namespace App\Model_PKTC\Model_PKTC_IRC;

use Illuminate\Database\Eloquent\Model;

class PaidStatusIrcPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'paids_status_irc';
}
