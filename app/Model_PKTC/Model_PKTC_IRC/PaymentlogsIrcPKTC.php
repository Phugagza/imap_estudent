<?php

namespace App\Model_PKTC\Model_PKTC_IRC;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsIrcPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'update_payment_irc_logs';
}
