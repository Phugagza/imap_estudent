<?php

namespace App\Model_PKTC\Model_PKTC_IRC;

use Illuminate\Database\Eloquent\Model;

class PaidIrcPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'paids_irc';
}
