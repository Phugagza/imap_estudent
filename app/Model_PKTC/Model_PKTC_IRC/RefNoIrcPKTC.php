<?php

namespace App\Model_PKTC\Model_PKTC_IRC;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class RefNoIrcPKTC extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'pktc';
    protected $table = 'ref_no_ircs';
}
