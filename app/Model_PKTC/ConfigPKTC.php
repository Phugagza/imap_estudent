<?php

namespace App\Model_PKTC;

use Illuminate\Database\Eloquent\Model;

class ConfigPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'configs';
}
