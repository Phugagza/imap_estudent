<?php

namespace App\Model_PKTC;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsPKTC extends Model
{
    protected $connection = 'pktc';
    protected $table = 'update_payment_logs';
}
