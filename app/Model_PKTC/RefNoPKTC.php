<?php

namespace App\Model_PKTC;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class RefNoPKTC extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'pktc';
    protected $table = 'ref_nos';
}
