<?php

namespace App\Model_TRANG;

use Illuminate\Database\Eloquent\Model;

class UserTRANG extends Model
{
    protected $connection = 'trang';
    protected $table = 'users';
}
