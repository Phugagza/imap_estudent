<?php

namespace App\Model_TRANG;

use Illuminate\Database\Eloquent\Model;

class ReceiptTRANG extends Model
{
    protected $connection = 'trang';
    protected $table = 'receipts';
}
