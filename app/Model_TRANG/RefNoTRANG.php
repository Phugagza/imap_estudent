<?php

namespace App\Model_TRANG;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefNoTRANG extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'trang';
    protected $table = 'ref_nos';
}
