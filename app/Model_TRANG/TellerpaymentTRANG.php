<?php

namespace App\Model_TRANG;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentTRANG extends Model
{
    protected $connection = 'trang';
    protected $table = 'tellerpayments';
    public $timestamps = false;
}
