<?php

namespace App\Model_TRANG;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsTRANG extends Model
{
    protected $connection = 'trang';
    protected $table = 'update_payment_logs';
}
