<?php

namespace App\Model_TRANG;

use Illuminate\Database\Eloquent\Model;

class ReceiptNoTRANG extends Model
{
    protected $connection = 'trang';
    protected $table = 'receipt_no_counts';
}
