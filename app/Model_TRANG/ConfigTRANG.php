<?php

namespace App\Model_TRANG;

use Illuminate\Database\Eloquent\Model;

class ConfigTRANG extends Model
{
    protected $connection = 'trang';
    protected $table = 'configs';
}
