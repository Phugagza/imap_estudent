<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Project extends Model
{
    public function college(){
        return $this->belongsTo('App\College','cl_id','id');
    }

    //bicec
    public function getCurrentDBBICEC()
    {
        return $this->where([['initials',Config('constants.bicec.initials')],['comcode',Config('constants.bicec.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }

    //iveb
    public function getCurrentDBIVEB()
    {
        return $this->where([['initials',Config('constants.iveb.initials')],['comcode',Config('constants.iveb.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }

    //nevia
    public function getCurrentDBNEVIA()
    {
        return $this->where([['initials',Config('constants.nevia.initials')],['comcode',Config('constants.nevia.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }

    //nivea
    public function getCurrentDBNIVEA()
    {
        return $this->where([['initials',Config('constants.nivea.initials')],['comcode',Config('constants.nivea.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }

    //ntckk
    public function getCurrentDBNTCKK()
    {
        return $this->where([['initials',Config('constants.ntckk.initials')],['comcode',Config('constants.ntckk.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }


    //pbtc
    public function getCurrentDBPBTC()
    {
        return $this->where([['initials',Config('constants.pbtc.initials')],['comcode',Config('constants.pbtc.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }

     //pktc
    public function getCurrentDBPKTC()
    {
        return $this->where([['initials',Config('constants.pktc.initials')],['comcode',Config('constants.pktc.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }

    //skntc
    public function getCurrentDBSKNTC()
    {
        return $this->where([['initials',Config('constants.skntc.initials')],['comcode',Config('constants.skntc.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }

    //sstc
    public function getCurrentDBSSTC()
    {
        return $this->where([['initials',Config('constants.sstc.initials')],['comcode',Config('constants.sstc.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }

    //trang
    public function getCurrentDBTRANG()
    {
        return $this->where([['initials',Config('constants.trang.initials')],['comcode',Config('constants.trang.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }

    //vei5
    public function getCurrentDBVEI5()
    {
        return $this->where([['initials',Config('constants.vei5.initials')],['comcode',Config('constants.vei5.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }

    //vein3
    public function getCurrentDBVEIN3()
    {
        return $this->where([['initials',Config('constants.vein3.initials')],['comcode',Config('constants.vein3.comcode')]])
                    ->orderBy('id', 'DESC')
                    ->first();
    }
}
