<?php

namespace App\Model_NTCKK;

use Illuminate\Database\Eloquent\Model;

class ReceiptNTCKK extends Model
{
    protected $connection = 'ntckk';
    protected $table = 'receipts';
}
