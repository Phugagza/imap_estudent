<?php

namespace App\Model_NTCKK;

use Illuminate\Database\Eloquent\Model;

class PaidNTCKK extends Model
{
    protected $connection = 'ntckk';
    protected $table = 'paids';
}
