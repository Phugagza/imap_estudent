<?php

namespace App\Model_NTCKK;

use Illuminate\Database\Eloquent\Model;

class PaidStatusNTCKK extends Model
{
    protected $connection = 'ntckk';
    protected $table = 'paids_status';
}
