<?php

namespace App\Model_NTCKK;

use Illuminate\Database\Eloquent\Model;

class ConfigNTCKK extends Model
{
    protected $connection = 'ntckk';
    protected $table = 'configs';
}
