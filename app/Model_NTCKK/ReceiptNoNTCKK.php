<?php

namespace App\Model_NTCKK;

use Illuminate\Database\Eloquent\Model;

class ReceiptNoNTCKK extends Model
{
    protected $connection = 'ntckk';
    protected $table = 'receipt_no_counts';
}
