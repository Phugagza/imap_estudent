<?php

namespace App\Model_NTCKK;

use Illuminate\Database\Eloquent\Model;

class UserNTCKK extends Model
{
    protected $connection = 'ntckk';
    protected $table = 'users';
}
