<?php

namespace App\Model_NTCKK;

use Illuminate\Database\Eloquent\Model;

class TellerpaymentNTCKK extends Model
{
    protected $connection = 'ntckk';
    protected $table = 'tellerpayments';
    public $timestamps = false;
}
