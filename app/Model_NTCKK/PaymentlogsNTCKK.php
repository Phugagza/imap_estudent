<?php

namespace App\Model_NTCKK;

use Illuminate\Database\Eloquent\Model;

class PaymentlogsNTCKK extends Model
{
    protected $connection = 'ntckk';
    protected $table = 'update_payment_logs';
}
