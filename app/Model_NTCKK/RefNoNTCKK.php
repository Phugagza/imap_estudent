<?php

namespace App\Model_NTCKK;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class RefNoNTCKK extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $connection = 'ntckk';
    protected $table = 'ref_nos';
}
